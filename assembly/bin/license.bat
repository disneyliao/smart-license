@echo off
set LICENSE_HOME=%cd%\..
set /p expire=expireTime:
set /p content=content or file:
java  -cp "%LICENSE_HOME%\lib\*" org.smartboot.license.server.LicenseServer %expire% %content%
pause
exit /b 0